Introduction
============

`Roster` is an `Ansible` inventory plugin in which you describe the hosts that comprise your inventory.

Groups are applied to the hosts, and hosts may belong to multiple groups. Groups themselves can belong to other groups. Group variables will be merged.

Host variables will take precedence over group variables. Group variables have precedence over global variables.

With the file inclusion feature, group and host declarations can be split amongst several files.

With the regex and ranges feature, hosts can be described once and expanded into several when the playbook runs.
