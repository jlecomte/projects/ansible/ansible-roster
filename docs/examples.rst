
Examples
========

A larger example Roster inventory for DebOps can be found here: `ansible roster example <https://gitlab.com/ansible/jlecomte/ansible-roster-example>`__

Example 1
---------

Here is the first Ansible inventory example from the Ansible documentation in Ansible inventory YAML format:

.. code-block:: yaml

  all:
    hosts:
      mail.example.com:
    children:
      webservers:
        hosts:
          foo.example.com:
          bar.example.com:
      dbservers:
        hosts:
          one.example.com:
          two.example.com:
          three.example.com:

Converted to Ansible Roster format:

.. code-block:: yaml

  ---
  plugin: roster

  groups:
    webservers:
    dbservers:

  hosts:
    mail.example.com:
    foo.example.com:
      groups:
        webservers:
    bar.example.com:
      groups:
        webservers:
    one.example.com:
      groups:
        dbservers:
    two.example.com:
      groups:
        dbservers:
    three.example.com:
      groups:
        dbservers:

Example 2
---------

Ansible inventory in Ansible inventory YAML format:

.. code-block:: yaml

  atlanta:
    hosts:
      host1:
      host2:
    vars:
      ntp_server: ntp.atlanta.example.com
      proxy: proxy.atlanta.example.com

Converted to Ansible Roster format:

.. code-block:: yaml

  ---
  plugin: roster

  groups:
    atlanta:
      vars:
        ntp_server: ntp.atlanta.example.com
        proxy: proxy.atlanta.example.com

  hosts:
    host1:
      groups:
        - atlanta
    host2:
      groups:
        - atlanta

Example 3
---------

Minimal viable inventory for DebOps:

.. code-block:: yaml

  ---
  plugin: roster

  groups:
    # avoid warning that group is not declared:
    - debops_all_hosts: { 'vars' }

  hosts:
    hostname:
      groups:
        - debops_all_hosts
