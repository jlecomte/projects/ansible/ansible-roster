
Groups
======

`Groups` are used to regroup similar settings for multiple hosts. Groups can inherit from other groups, and a host can belong to multiple groups.

A group is defined by it's name and can contain a parent group section and variables.

.. code-block:: yaml

  ---
  plugin: roster

  group_one:
    parents:
      -  group_two
    vars:
      foobar: "one"

Keys
----

The `groups` sections support the following keys:

* `vars`
* `parents`

Inheritance and priority
------------------------

A group can define multiple parents. For each declared parent, the group will inherit variables.

In the case of scalar variables, parent variables are overwritten by child variables. In all other cases, they are merged (cf. next section).

.. code-block:: yaml

  ---
  plugin: roster

  groups:
    first:
      vars:
        foobar: "low priority"

    second:
      parents:
        - first
      vars:
        foobar: "high priority"

  hosts:
    # foobar will be "high priority"
    foobar.example.com:
      groups:
        - second


Inheritance and merging
------------------------

When a group variable is list or a dictionary, then the variables are merged with children variables.

This will also be the case for multiple parents for a group or multiple groups for a host.

.. code-block:: yaml

  ---
  plugin: roster

  groups:
    french_users:
      vars:
        users:
          - name: "french"
            state: "present"
    english_users:
      vars:
        users:
          - name: "english"
            state: "present"

  hosts:
    # foobar will contain both users
    foobar.example.com:
      groups:
        - french_users
        - english_users
