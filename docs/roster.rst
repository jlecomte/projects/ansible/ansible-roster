
Roster Inventory File
=====================

The roster inventory file is a yaml formatted file, usually named :file:`roster.yml`. It is usually located either at the root of your project, or in a directory named :file:`inventory`. This file acts as the entrypoint to the definition of your infrastructure.

In order for the file to be recognized as a roster inventory file, `Ansible` must be directed via it's configuration file to include the plugin, and the roster inventory file must be in the correct format.


Requirements
------------
The below Python3 requirements are needed on the local controller node that executes this inventory.

- boltons
- cerberus
- exrex


Ansible configuration
---------------------

Edit your :file:`ansible.cfg` in order to specify the usage of the plugin and the name of your roster inventory file.

.. code-block:: ini

  [defaults]
  # Filename requires a 'yml' or 'yaml' extension.
  # 'roster.yml' is the default filename.
  inventory = roster.yml

  [inventory]
  # You must enable the roster plugin if the default
  # 'auto' does not work for you
  enabled = roster
  # Use 'roster' if installed via the Python package,
  # Use 'julien_lecomte.roster.roster' if installed via Ansible Galaxy
  enable_plugins = julien_lecomte.roster.roster


DebOps configuration
--------------------

Edit your :file:`.debops.cfg` in order to specify the usage of the plugin and the name of your roster inventory file then run ``debops project refresh``.

.. code-block::

  [ansible inventory]
  enabled = roster
  # Use 'roster' if installed via the Python package,
  # Use 'julien_lecomte.roster.roster' if installed via Ansible Galaxy
  enable_plugins = julien_lecomte.roster.roster


File Format
-----------

An inventory file explicitly named :file:`roster.yml` will be automatically recognized as a `roster` inventory. In all other cases, the inventory file must contain a variable so that `Ansible` recognizes the file as being a `roster` inventory.

In the latter case, for your `roster` inventory to be accepted by the plugin, you need to add key `plugin` with value `roster`.

A minimum of one host will still be required for the inventory to be valid.

.. code-block:: yaml

  ---
  # Tell ansible that this yaml file is a roster file:
  plugin: roster


File inclusion
--------------

The roster format allows the inclusion of other roster files via the `include` keyword. Files can be included by exact filename match, or by file globbing match.

No inventory files will be autodiscovered and included by itself; files must be explicitly included via the `include` keyword.

Included files can include additional files, within the limit of one `include` keyword per yaml file.

Paths are relative to the main inventory file but absolute paths are allowed.

.. code-block:: yaml

  ---
  plugin: roster

  include:
    # include an exact file match
    - distrib/debian.yml

    # include with wildcard
    - hosts/*.yml

The location of the `include` keyword in the file, before or after other keys, has no impact on the resulting inventory and includes will always be parsed first before keys.


Keys
----

The roster file supports the following root keys:

* `plugin`
* `include`
* `hosts`
* `groups`
* `vars`
