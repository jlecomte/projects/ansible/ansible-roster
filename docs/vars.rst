
Variables
=========

Variables can be specified at all three levels: at root as a global variable, as a group variable, or as a host variable.

Priority
--------

Variables at the root of the document will have lowest priority. In other words, any redeclaration of this variable will mask the initial value. This is the best place to add default values.

There is no merging of variables between global, group and host variables.

Priority from least to most:

* root
* `groups`
* `hosts`

.. code-block:: yaml

  ---
  plugin: roster

  vars:
    var_one: "lowest priority"
    var_two: "overwritten by group first"
    var_third: "overwritten by host"
    no_merging: [0]

  groups:
    first:
      vars:
        var_two: "mid priority"
        var_third: "overwritten by host"
        no_merging: [1]

  hosts:
    # foobar will have no_merging variable equal "[ 2 ]" and not "[0, 1, 2]"
    foobar.example.com:
      groups:
        - first
      vars:
        var_third: "highest priority"
        no_merging: [2]
