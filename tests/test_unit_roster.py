from operator import attrgetter

from boltons.iterutils import remap
from pytest import mark, raises

from tests import assets, get_expected_results

try:
    # If ever ansible changes their internal api, don't break
    from ansible.constants import INTERNAL_STATIC_VARS
except ImportError:
    from ansible.cli.inventory import INTERNAL_VARS as INTERNAL_STATIC_VARS


def to_dict(inventory):
    """
    Return inventory as a dict. It can then be transformed directly to
    valid ansible JSON inventory.

    :params inventory: An InventoryData object
    :returns: Dictionnary
    """

    def visit(_path, key, value):
        # if it's a list, but it's empty, then drop it
        if isinstance(value, list) and not value:
            return False
        if isinstance(value, dict) and not value and key != "hostvars":
            return False
        return True

    # loosely adapted from ansible/cli/inventory.py
    # too bad there's no public API for this conversion
    retval = {"_meta": {"hostvars": {}}}
    seen = set()

    def format_group(group):
        retval = {
            group.name: {
                # since a host belongs to either a group, or the group 'ungrouped', there are no hosts in group 'all'
                "hosts": [h.name for h in sorted(group.hosts, key=attrgetter("name")) if group.name != "all"],
                "children": [],
            },
        }
        for subgroup in sorted(group.child_groups, key=attrgetter("name")):
            retval[group.name]["children"].append(subgroup.name)
            if subgroup.name not in seen:
                retval.update(format_group(subgroup))
                seen.add(subgroup.name)
        return retval

    top = inventory.groups.get("all")
    retval.update(format_group(top))

    # populate meta
    for host_name, host in inventory.hosts.items():
        host_vars = {}
        for group in sorted(host.groups, key=attrgetter("name")):
            # print(group.name, " =>", group.vars)
            host_vars.update(group.vars)
        host_vars.update(host.vars)
        host_vars = {k: v for k, v in host_vars.items() if k not in INTERNAL_STATIC_VARS}
        if host_vars:
            retval["_meta"]["hostvars"][host_name] = host_vars

    retval = remap(retval, visit=visit)
    return retval


@mark.parametrize(
    "path,expected",
    [
        ("roster.yml", True),
        ("roster.yaml", True),
        ("roster.xml", False),
    ],
)
@mark.order(0)
def test_roster_plugin_misc(inventory_module, path, expected):
    roster = inventory_module

    verified = roster.verify_file(path)
    assert verified == expected


@mark.parametrize("src", assets("assets/units", r"^.*[^\.inc]\.yml$"))
@mark.order(1)
def test_roster_plugin(inventory_module, src):
    roster = inventory_module
    verified = roster.verify_file(src)

    expected = get_expected_results(src)

    if expected and verified:
        roster.parse(roster.inventory, roster.loader, src, False)
        results = to_dict(roster.inventory)
        assert results == expected, f'File "{src}" failed unit-test'
    elif not verified:
        assert expected is None, f'File "{src}" failed unit-test'
    else:
        with raises(SystemExit):
            roster.parse(roster.inventory, roster.loader, src, False)
