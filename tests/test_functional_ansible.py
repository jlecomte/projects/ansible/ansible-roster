import json
import os
import shutil
import subprocess

from pytest import mark, skip

from tests import assets, get_expected_results


@mark.parametrize("src", assets("assets/functional", r"^.*[^\.inc]\.yml$"))
@mark.order(2)
def test_roster_plugin_success(src):
    assert os.path.exists(src), "File not found"
    cmd = shutil.which("ansible-inventory") + " --list -i " + src
    retval = subprocess.run(cmd.split(), shell=False, capture_output=True, text=True, env=os.environ, check=False)
    retval.check_returncode()

    # ansible-inventory seems to always return 0, so parse output
    stdout = None
    if retval.stderr.find("ignoring it as an ansible.cfg") != -1:
        # when ansible-inventory is run from a world writable directory, it will ignore
        # the configuration
        skip("Ansible is being run from a world writable directory")
    elif retval.stderr.find("No inventory was parsed,") != -1:
        stdout = None
    else:
        stdout = json.loads(retval.stdout)

    expected = get_expected_results(src)
    assert stdout == expected, f"\nstdout =\n{retval.stdout}\n\nstderr=\n{retval.stderr}\n"
