import yaml
from ansible.inventory.data import InventoryData
from ansible.parsing.dataloader import DataLoader
from ansible.template import Templar
from pytest import fixture

try:
    from tests._plugins.roster import InventoryModule
except ImportError:
    from ansible.plugins.inventory.roster import InventoryModule


class MockDataLoader(DataLoader):
    def load_from_file(self, file_name, cache=True, unsafe=False, _json_only=False):
        with open(file_name, encoding="UTF-8") as fd:
            data = yaml.safe_load(fd)
        return data


@fixture(scope="function")
def inventory_module():
    roster = InventoryModule()
    roster.inventory = InventoryData()
    roster.inventory.name = roster.NAME
    roster.loader = MockDataLoader()
    roster.templar = Templar(None)
    return roster
