import re

import yaml
from pytest import mark, raises

try:
    from tests._plugins.roster import SCHEMA, _recompose_host
except ImportError:
    from ansible.plugins.inventory.roster import SCHEMA, _recompose_host

from tests import MockDisplay

schema = yaml.safe_load(SCHEMA)
hostname_re = re.compile(schema["hosts"]["keysrules"]["regex"])


@mark.parametrize(
    "hostname, expected",
    [
        # fmt: off
        ("host-name_01.example", r"host\-name_01\.example"),
        #  ranges
        ("foobar", "foobar"),
        ("foo[0:9]bar", "foo([0123456789])bar"),
        ("foo[5:9]bar", "foo([56789])bar"),
        ("foo[05:09]bar", "foo(0[56789])bar"),
        # regex group
        ("foo(ab|ac)bar", "foo(ab|ac)bar"),
        ("foo(ab|ac)", "foo(ab|ac)"),
        ("(ab|ac)bar", "(ab|ac)bar"),
        ("foo(ab|ac)bar.example.com", r"foo(ab|ac)bar\.example\.com"),
        ("foo(ab|ac)bar.([ab]).example.com", r"foo(ab|ac)bar\.([ab])\.example\.com"),
        #
        ("foo(ab|ac)bar[001:2].example.com", r"foo(ab|ac)bar(00[12])\.example\.com"),
        # exceptions
        ("foo[5:4]", None),
        ("foo5:4]", None),
        # fmt: on
    ],
)
@mark.order(0)
def test_yaml_validation_hostname(hostname, expected):
    assert bool(hostname_re.match(hostname))

    if expected is None:
        with raises(Exception):
            hostname = _recompose_host(hostname, MockDisplay())
    else:
        hostname = _recompose_host(hostname, MockDisplay())
        assert hostname == expected, f"Generated hostname '{hostname}' does not match expected result: '{expected}'"
